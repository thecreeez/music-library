<?php

namespace App\Models;

use App\Models\Album;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Artist extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'image',
  ];

  public function albums(): HasMany
  {
    return $this->hasMany(Album::class);
  }
}

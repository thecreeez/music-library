<?php

namespace App\Repositories;

use App\Models\Album;
use App\Models\Artist;

class AlbumRepository implements AlbumRepositoryInterface
{
    public function create(Artist $artist, string $name, string $description, string $image): ?Album
    {
        return Album::create([
            'artist_id' => $artist->id,
            'name' => $name,
            'description' => $description,
            'image' => $image,
        ]);
    }

    public function findById(int $id): ?Album
    {
        return Album::where('id', $id)->first();
    }

    public function findByName(string $name): ?Album
    {
        return Album::where('name', $name)->first();
    }

    public function removeById(int $id): bool
    {
        $album = $this->findById($id);

        if ($album === null) {
            return false;
        }

        return $album->delete();
    }
}
<?php

namespace App\Repositories;

use App\Models\Artist;

class ArtistRepository implements ArtistRepositoryInterface
{
  public function create(string $name, string $image): ?Artist
  {
    return Artist::create([
      'name' => $name,
      'image' => $image,
    ]);
  }

  public function findById(int $id): ?Artist
  {
    return Artist::where('id', $id)->first();
  }

  public function findByName(string $name): ?Artist
  {
    return Artist::where('name', $name)->first();
  }


  public function removeById(int $id): bool
  {
    $artist = $this->findById($id);

    if ($artist === null) {
      return false;
    }

    return $artist->delete();
  }
}
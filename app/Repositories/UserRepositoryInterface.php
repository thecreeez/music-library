<?php

namespace App\Repositories;

use App\Models\User;

interface UserRepositoryInterface
{
  public function create(string $login, string $password): ?User;
  public function findByLogin(string $login): ?User;
  public function removeByLogin(string $login): bool;
}
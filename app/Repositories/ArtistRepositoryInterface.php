<?php

namespace App\Repositories;

use App\Models\Artist;

interface ArtistRepositoryInterface
{
  public function create(string $name, string $image): ?Artist;
  public function findById(int $id): ?Artist;
  public function findByName(string $name): ?Artist;
  public function removeById(int $id): bool;
}
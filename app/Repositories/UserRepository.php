<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
  public function create(string $login, string $password): ?User
  {
    return User::create([
      'login' => $login,
      'password' => $password
    ]);
  }

  public function findByLogin(string $login): ?User
  {
    return User::where('login', $login)->first();
  }

  public function removeByLogin(string $login): bool
  {
    $user = $this->findByLogin($login);

    if ($user === null) {
      return false;
    }

    return $user->delete();
  }
}

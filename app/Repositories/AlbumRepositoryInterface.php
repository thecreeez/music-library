<?php

namespace App\Repositories;

use App\Models\Album;
use App\Models\Artist;

interface AlbumRepositoryInterface
{
    public function create(Artist $artist, string $name, string $description, string $image): ?Album;
    public function findById(int $id): ?Album;
    public function findByName(string $name): ?Album;
    public function removeById(int $id): bool;
}
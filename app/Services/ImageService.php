<?php

namespace App\Services;

use Http;

use App\Services\RandomCatApiService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Client\Response;

use Illuminate\Http\Client\Pool;

class ImageService {
    public function __construct(
        protected RandomCatApiService $randomCatApiService,
    ) {}

    public function download(string $url): string
    {
        $imageUuid = Str::uuid()->toString();

        // Запасной запрос на случай если первая апишка не ответит
        $urls = [$url, $this->randomCatApiService->get()];

        $responses = Http::pool(fn(Pool $pool) => [
            // Last.fm API
            $pool->withOptions(['verify' => false])->withQueryParameters(['api_key' => config('api.token')])->connectTimeout(5)->get($urls[0]),

            // Random cat API
            $pool->withOptions(['verify' => false])->connectTimeout(5)->get($urls[1]),
        ]);

        $isImageRecieved = false;
        foreach ($responses as $i => $response) {
            if (get_class($response) === Response::class && !isset($file)) {
                $file = $response;
                
                $url = $urls[$i];
                $isImageRecieved = true;
            }
        }

        if (!$isImageRecieved) {
            return 'placeholder.png';
        }
        
        $imageExtension = pathinfo($url)['extension'];
        $fileName = $imageUuid . '.' . $imageExtension;

        Storage::disk('images')->put($fileName, $file);
        return $fileName;
    }

    public function remove(string $name): bool
    {
        if ($name === 'placeholder.png') {
            return true;
        }

        return Storage::disk('images')->delete($name);
    }
}

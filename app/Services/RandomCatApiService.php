<?php

namespace App\Services;

use Http;
use Config;

class RandomCatApiService
{
    public function get(): string
    {
        $apiUrl = config('api.cat_url_api');
        $result = Http::withOptions(['verify' => false])->retry(3, 500)->get($apiUrl);
        
        return $result[0]['url'];
    }
}

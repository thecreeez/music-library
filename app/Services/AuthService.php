<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;

use Illuminate\Support\Facades\Auth;

class AuthService
{
  public function __construct(
    protected UserRepository $userRepository,
  ) {}

  public function login($request): bool 
  {
    return Auth::attempt($request);
  }

  public function register($request): bool 
  {
    $user = $this->userRepository->create($request['login'], $request['password']);
    Auth::login($user);

    return true;
  }

  public function logout(): bool
  {
    if (Auth::check()) {
        Auth::logout();
        return true;
    }

    return false;
  }

  public function getUser(): ?User
  {
    return Auth::user();
  }
}

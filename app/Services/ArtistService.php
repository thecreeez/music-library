<?php

namespace App\Services;

use App\Models\Artist;
use App\Repositories\AlbumRepository;
use Http;

use App\Repositories\ArtistRepository;
use App\Services\AuthService;
use App\Services\ImageService;
use App\Services\LastFMApiService;

use Illuminate\Support\Facades\Log;

class ArtistService
{
    public function __construct(
        protected ArtistRepository $artistRepository,
        protected AlbumRepository $albumRepository,
        protected ImageService $imageService,
        protected AuthService $authService,
        protected LastFMApiService $lastFMApiService,
    ) {
    }

    public function create(string $name): ?Artist
    {
        $artistsFromLastFM = $this->lastFMApiService->getArtist($name);
        $imageUrl = null;

        $fileName = 'placeholder.png';

        if (count($artistsFromLastFM) > 0) {
            $imageUrl = $artistsFromLastFM[0]['image'][2]['#text'];
            $fileName = $this->imageService->download($imageUrl);
        }
        
        $artist = $this->artistRepository->create($name, $fileName);

        if ($artist) {
            Log::channel('artists_updates')->info('Исполнитель ' . $name . ' (Фото: ' . $imageUrl . ') добавлен пользователем ['. $this->authService->getUser()['login'] . ']');
        }

        return $artist;
    }

    public function update(string $id, string $name): bool
    {
        $artist = $this->artistRepository->findById($id);

        if ($artist === null) {
            return false;
        }

        if ($artist->name !== $name) {
            Log::channel('artists_updates')->
                info('Изменено имя артиста ' . $artist->id . '. Было: ' . $artist->name . ', стало:' . $name . '. пользователь: [' . $this->authService->getUser()['login'] . ']');
            $artist->name = $name;
        }

        $artist->save();
        return true;
    }

    public function remove(int $id): bool
    {
        $artist = $this->artistRepository->findById($id);

        if ($artist !== null) {
            foreach($artist->albums as $album) {
                $this->imageService->remove($album->image);
                $this->albumRepository->removeById($album->id);
            }

            $this->imageService->remove($artist->image);

            Log::channel('artists_updates')->info('Исполнитель ' . $artist->name . ' удален пользователем [' . $this->authService->getUser()['login'] . ']');
            return $this->artistRepository->removeById($id);
        }

        return false;
    }

    public function findByName(string $name): ?Artist
    {
        return $this->artistRepository->findByName($name);
    }
}

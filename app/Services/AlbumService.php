<?php

namespace App\Services;

use App\Repositories\AlbumRepository;

use App\Models\Album;
use App\Services\ArtistService;
use App\Services\AuthService;
use App\Services\ImageService;
use App\Services\LastFMApiService;

use Illuminate\Support\Facades\Log;

class AlbumService
{
    public function __construct(
        protected AlbumRepository $albumRepository,
        protected ArtistService $artistService,
        protected ImageService $imageService,
        protected AuthService $authService,
        protected LastFMApiService $lastFMApiService,
    ) {
    }

    public function create(string $name, string $description, string $artistName): ?Album
    {
        $albumsFromLastFM = $this->lastFMApiService->getAlbum($name . ' ' . $artistName);

        $fileName = 'placeholder.png';
        if (count($albumsFromLastFM) > 0) {
            $imageUrl = $albumsFromLastFM[0]['image'][2]['#text'];
            $fileName = $this->imageService->download($imageUrl);
        }

        $artist = $this->artistService->findByName($artistName);

        if ($artist === null) {
            $artist = $this->artistService->create($artistName);
        }

        $album = $this->albumRepository->create($artist, $name, $description, $fileName);

        if ($album !== null) {
            Log::channel('albums_updates')->info('Альбом ' . $name . ' от ' . $artist->name . ' добавлен пользователем [' . $this->authService->getUser()['login'] . ']');
        }

        return $album;
    }

    public function update(string $id, string $artistName, string $name, string $description): bool
    {
        $album = $this->albumRepository->findById($id);

        if ($album === null) {
            return false;
        }

        $album->name = $name;

        if ($album->artist->name !== $artistName) {
            $artist = $this->artistService->findByName($artistName);

            if ($artist === null) {
                $artist = $this->artistService->create($artistName);
            }

            $album->artist_id = $artist->id;
        }

        $album->description = $description;

        if ($album->save()) {
            Log::channel('albums_updates')->
                info('Изменен альбом ' . $album->id . '. пользователь: [' . $this->authService->getUser()['login'] . ']');

            Log::channel('albums_updates')->
                info('Описание: ' . $album->description);

            Log::channel('albums_updates')->
                info('Картинка: ' . $album->image);

            Log::channel('albums_updates')->
                info('Исполнитель: ' . $album->artist->name);
            return true;
        }
        return false;
    }

    public function remove(int $id): bool
    {
        $album = $this->albumRepository->findById($id);

        if ($album !== null) {
            $this->imageService->remove($album->image);

            Log::channel('albums_updates')->info('Альбом ' . $album->name . ' удален пользователем [' . $this->authService->getUser()['login'] . ']');
            return $this->albumRepository->removeById($id);
        }

        return false;
    }
}

<?php

namespace App\Services;

use Http;
use Config;

class LastFMApiService
{
    public function getArtist(string $name): array
    {
        $apiKey = config('api.token');
        $apiUrl = config('api.url_api');

        $result = Http::withOptions(['verify' => false])->withQueryParameters([
            'method' => 'artist.search',
            'artist' => $name,
            'api_key' => $apiKey,
            'limit' => 5,
            'format' => 'json',
        ])->get($apiUrl);

        return $result['results']['artistmatches']['artist'];
    }

    public function getAlbum(string $name): array
    {
        $apiKey = config('api.token');
        $apiUrl = config('api.url_api');

        $result = Http::withOptions(['verify' => false])->withQueryParameters([
            'method' => 'album.search',
            'album' => $name,
            'api_key' => $apiKey,
            'limit' => 5,
            'format' => 'json',
        ])->get($apiUrl);

        return $result['results']['albummatches']['album'];
    }
}

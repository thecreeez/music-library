<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumEditRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => ['required', 'exists:albums'],
            'artist' => ['required'],
            'name' => ['required', 'max:30'],
            'description' => ['required', 'max:500'],
        ];
    }
}

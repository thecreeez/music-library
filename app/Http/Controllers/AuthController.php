<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class AuthController extends Controller
{
    public function __construct(
        protected AuthService $authService,
    ) {}
    
    public function login(LoginRequest $request): RedirectResponse 
    {
        $validated = $request->validated();

        $response = $this->authService->login($validated);

        if (!$response) {
            return redirect(route('login'))->withErrors(['error' => 'Неверный логин или пароль.']);
        }

        return redirect(route('index'))->with('status', 'Вы успешно вошли в аккаунт под логином '.$validated['login']);
    }

    public function register(RegisterRequest $request): RedirectResponse 
    {
        $validated = $request->validated();

        $response = $this->authService->register($validated);

        if ($response === null) {
            return redirect(route('register'))->withErrors(['error' => 'Данный логин уже занят.']);
        }

        return redirect(route('index'))->with('status', 'Вы успешно зарегистрировались под логином '.$validated['login']);
    }

    public function logout(Request $request): RedirectResponse
    {
        if (!$this->authService->logout()) {
            return redirect(route($request->route()))->withErrors(['error' => 'Не удалось выйти из аккаунта: вы и так не авторизованы']);
        }

        return redirect(route('index'))->with('status', 'Вы успешно вышли из аккаунта.');
    }
}

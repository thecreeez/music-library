<?php

namespace App\Http\Controllers;

use App\Services\LastFMApiService;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct(
        protected LastFMApiService $lastFMApiService,
    ) {}

    public function searchArtist(Request $request): array
    {
        return $this->lastFMApiService->getArtist($request->name);
    }

    public function getAlbum(Request $request): array
    {
        return $this->lastFMApiService->getAlbum($request->name);
    }
}

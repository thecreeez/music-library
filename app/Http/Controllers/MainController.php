<?php

namespace App\Http\Controllers;

use App\Services\RandomCatApiService;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class MainController extends Controller
{
    public function __construct(
        protected RandomCatApiService $randomCatApiService,
    ) {}
    public function index(Request $request): View
    {
        return view('index', ['catUrl' => $this->randomCatApiService->get()]);
    }
}

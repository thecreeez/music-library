<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumCreateRequest;
use App\Http\Requests\AlbumEditRequest;
use App\Http\Requests\AlbumRemoveRequest;
use App\Models\Album;
use App\Models\Artist;
use App\Services\AlbumService;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;

class AlbumController extends Controller
{
    public function __construct(
        protected AlbumService $albumService,
    ) {}

    public function index(Request $request): View
    {
        $albums = Album::whereHas('artist', function ($q) use($request) {
            $q->where('name', 'like', '%' . $request->name . '%');
        })->cursorPaginate(5);
        
        return view('albums', ['albums' => $albums]);
    }

    public function create(AlbumCreateRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $this->albumService->create($validated['name'], $validated['description'], $validated['artist']);

        return redirect(route('albums'))->with('status', 'Альбом успешно создан.');
    }

    public function update(AlbumEditRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $this->albumService->update($validated['id'], $validated['artist'], $validated['name'], $validated['description']);

        return redirect(route('albums'))->with('status', 'Альбом успешно изменен.');
    }

    public function remove(AlbumRemoveRequest $request): RedirectResponse
    {
        $validated = $request->validated();

        if ($this->albumService->remove($validated['id'])) {
            return redirect(route('albums'))->with('status', 'Альбом успешно удален.');
        } else {
            return redirect(route('albums'))->withErrors(['error' => 'Не удалось удалить альбом.']);
        }
    }

    public function show(Request $request): View
    {
        return view('album-edit', ['album' => Album::findOrFail($request->id)]);
    }
}

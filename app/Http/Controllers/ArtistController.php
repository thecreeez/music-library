<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Services\ArtistService;
use App\Http\Requests\ArtistCreateRequest;
use App\Http\Requests\ArtistEditRequest;
use App\Http\Requests\ArtistRemoveRequest;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;

class ArtistController extends Controller
{
    public function __construct(
        protected ArtistService $artistService,
    ) {
    }

    public function index(Request $request): View
    {
        return view('artists', ['artists' => Artist::where('name', 'like', '%' . $request->name . '%')->cursorPaginate(5)]);
    }

    public function create(ArtistCreateRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $this->artistService->create($validated['name']);

        return redirect(route('artists'))->with('status', 'Исполнитель успешно создан.');
    }

    public function update(ArtistEditRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $this->artistService->update($validated['id'], $validated['name']);

        return redirect(route('artists'))->with('status', 'Исполнитель успешно изменен.');
    }

    public function remove(ArtistRemoveRequest $request): RedirectResponse
    {
        $validated = $request->validated();

        if ($this->artistService->remove($validated['id'])) {
            return redirect(route('artists'))->with('status', 'Исполнитель успешно удален.');
        } else {
            return redirect(route('artists'))->withErrors(['error' => 'Не удалось удалить исполнителя.']);
        }
    }

    public function show(Request $request): View
    {
        return view('artist-edit', ['artist' => Artist::findOrFail($request->id)]);
    }
}

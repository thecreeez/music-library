<?php
return [
    'token' => env('LAST_FM_API_KEY'),
    'url_api' => 'https://ws.audioscrobbler.com/2.0/',
    'cat_url_api' => 'https://api.thecatapi.com/v1/images/search',
];
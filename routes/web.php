<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

/**
 * INDEX
 */
Route::get('/', [MainController::class, 'index'])->name('index');

/**
 * ALBUMS
 */
Route::get('/albums', [AlbumController::class, 'index'])->name('albums');
Route::get('/album/{id}', [AlbumController::class, 'show'])->name('album');
Route::view('/album', 'album-add')->name('album-add');

/**
 * ARTISTS
 */
Route::get('/artists', [ArtistController::class, 'index'])->name('artists');
Route::get('/artist/{id}', [ArtistController::class, 'show'])->name('artist');
Route::view('/artist', 'artist-add')->name('artist-add');

/**
 * AUTH
 */
Route::view('/register', 'register')->name('register');
Route::view('/login', 'login')->name('login');

Route::controller(AuthController::class)->group(function() {
    Route::post('/login', 'login')->name('login');
    Route::post('/register', 'register')->name('register');
    Route::get('/logout', 'logout')->name('logout');
});

Route::middleware('auth')->group(function () {
    Route::put('/artist', [ArtistController::class, 'create'])->name('artist');
    Route::patch('/artist', [ArtistController::class, 'update'])->name('artist');
    Route::delete('/artist', [ArtistController::class, 'remove'])->name('artist');

    Route::put('/album', [AlbumController::class, 'create'])->name('album');
    Route::patch('/album', [AlbumController::class, 'update'])->name('album');
    Route::delete('/album', [AlbumController::class, 'remove'])->name('album');

    /**
     * API
     */
    Route::controller(ApiController::class)->prefix('/api')->
        group(function() {
            Route::get('/search-artist', 'searchArtist');
            Route::get('/search-album', 'getAlbum');
        });
});
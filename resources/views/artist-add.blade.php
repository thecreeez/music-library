@extends('parts.app')

@section('title')Добавление исполнителя@endsection

@section('content')
<div class="row">
    <div class="col">
        <h4>Добавить исполнителя</h4>
        <form action="{{ route('artist') }}" method="post">
            @csrf
            @method('PUT')

            <div class="mb-3">
                <label for="name" class="form-label">Имя исполнителя</label>
                <input type="text" class="form-control" id="name" name="name" data-bs-toggle="dropdown" value="{{ old('name') }}">
                <ul class="dropdown-menu" aria-labelledby="inputName" id="namesByApi"></ul>
                * В случае если не получается взять картинку из last.fm, будет использоваться api с случайными котятами
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
    <div class="col">
        <div class="d-flex h-100 flex-column justify-content-center">
            <h4>Предпросмотр</h4>

            <div class="card mb-3 row shadow-sm" style="max-width: 540px;">
                <div class="row g-0" style="max-height: 200px">
                    <div class="col-md-4" style="max-height: 200px">
                        <img id="lookoutPicture" src="{{ Storage::disk('images')->url('placeholder.png') }}" class="img-fluid rounded-start h-100"
                            alt="Картинка артиста">
                    </div>
                    <div class="col-md-8" style="max-height: 200px">
                        <div class="card-body">
                            <h5 class="card-title" id="lookoutName">Имя исполнителя</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/artist-app.js"></script>
@endsection
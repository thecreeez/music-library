@extends('parts.app')

@section('title')Изменение исполнителя@endsection

@section('content')
<div class="row">
    <div class="col">
        <h4>Изменение исполнителя</h4>
        <form action="{{ route('artist') }}" method="post">
            @csrf
            @method('PATCH')

            <input type="hidden" name="id" value="{{ $artist->id }}">

            <div class="mb-3">
                <label for="name" class="form-label">Имя исполнителя</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $artist->name }}" data-bs-toggle="dropdown">
                <ul class="dropdown-menu" aria-labelledby="inputName" id="namesByApi"></ul>
                * В случае если не получается взять картинку из last.fm, будет использоваться api с случайными котятами
            </div>

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
        <form action="{{ route('artist') }}" method="post">
            @csrf
            @method('DELETE')

            <input type="hidden" name="id" value="{{ $artist->id }}">
            <button type="submit" class="btn btn-danger">Удалить</button>
        </form>
    </div>
    <div class="col">
        <div class="d-flex h-100 flex-column justify-content-center">
            <h4>Предпросмотр</h4>

            <div class="card mb-3 row shadow-sm" style="max-width: 540px;">
                <div class="row g-0" style="max-height: 200px">
                    <div class="col-md-4" style="max-height: 200px">
                        <img id="lookoutPicture" src="{{ Storage::disk('images')->url($artist->image) }}" class="img-fluid rounded-start h-100"
                            alt="Картинка артиста">
                    </div>
                    <div class="col-md-8" style="max-height: 200px">
                        <div class="card-body">
                            <h5 class="card-title" id="lookoutName">{{ $artist->name }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/artist-app.js"></script>
@endsection
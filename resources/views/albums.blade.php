@extends('parts.app')

@section('title')Альбомы@endsection

@section('content')
<div class="d-flex flex-column align-items-center">
    <form action="{{ route('albums') }}" method="GET" class="filter-input input-group mb-3">
        <input type="text" name="name" class="form-control" placeholder="Имя исполнителя">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Фильтр</button>
        </div>
    </form>

    @foreach($albums as $album)
        <div class="card mb-3 row shadow-sm" style="width: 540px;">

            <div class="row g-0" style="max-height: 200px">
                <div class="col-md-4" style="max-height: 200px">
                    <a href="/album/{{ $album->id }}"><img src="{{ Storage::disk('images')->url($album->image) }}"
                            class="img-fluid rounded-start h-100 album-picture" alt="..."></a>
                </div>
                <div class="col-md-8" style="max-height: 200px">
                    <div class="card-body">
                        <h5 class="card-title">{{ $album->name }}</h5>
                        <a href="/artist/{{ $album->artist->id }}"><h6 class="card-text">{{ $album->artist->name }}</h6></a>
                        <p class="card-text" style="max-height: 70px; text-overflow: ellipsis; overflow: hidden">{{
                            $album->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<nav aria-label="Page navigation">
</nav>
@endsection
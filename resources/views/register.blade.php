@extends('parts.app')

@section('title')Регистрация@endsection

@section('content')
<form action="{{ route('register') }}" method="post">
    @csrf

    <div class="mb-3">
        <label for="login" class="form-label">Логин</label>
        <input type="text" class="form-control" id="login" name="login">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Пароль</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>
    <div class="mb-3">
        <label for="password_confirmation" class="form-label">Подтвердить пароль</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
    </div>
    <div class="mb-3">
        <a href="{{ route('login') }}">У меня уже есть аккаунт</a>
    </div>

    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
</form>
@endsection
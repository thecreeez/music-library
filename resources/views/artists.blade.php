@extends('parts.app')

@section('title')Исполнители@endsection

@section('content')
<div class="d-flex flex-column align-items-center">

    <form action = "{{ route('artists') }}" method="GET" class="filter-input input-group mb-3">
        <input type="text" name="name" class="form-control" placeholder="Имя исполнителя">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Фильтр</button>
        </div>
    </form>

    @foreach($artists as $artist)
        <div class="card mb-3 row shadow-sm" style="width: 300px;">
            <div class="row g-0" style="max-height: 150px">
                <div class="col-md-4" style="max-height: 150px">
                    <a href="/artist/{{ $artist->id }}"><img src="{{ Storage::disk('images')->url($artist->image) }}" 
                        class="img-fluid rounded-start h-100 album-picture" alt="..."></a>
                </div>
                <div class="col-md-8" style="max-height: 150px">
                    <div class="card-body">
                        <h5 class="card-title">{{ $artist->name }}</h5>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <nav aria-label="Page navigation">
        {{ $artists->links() }}
    </nav>
</div>
@endsection
@extends('parts.app')

@section('title')Изменение альбома@endsection

@section('content')
<div class="row">
    <div class="col">
        <h4>Изменить альбом</h4>
        <form action="{{ route('album') }}" method="post">
            @csrf
            @method('PATCH')

            <input type="hidden" name="id" value="{{ $album->id }}">

            <div class="mb-3">
                <label for="inputArtist" class="form-label">Исполнитель</label>
                <input type="text" class="form-control" id="inputArtist" name="artist" value="{{ $album->artist->name }}">
            </div>
            <div class="mb-3">
                <label for="inputName" class="form-label">Альбом</label>
                <input type="text" class="form-control" id="inputName" name="name" data-bs-toggle="dropdown" value="{{ $album->name}}">
                <ul class="dropdown-menu" aria-labelledby="inputName" id="namesByApi"></ul>
            </div>
            <div class="mb-3">
                <label for="inputDescription" class="form-label">Описание</label>
                <textarea class="form-control" id="inputDescription" rows="3" name="description">{{ $album->description }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
        <form action="{{ route('album') }}" method="post">
            @csrf
            @method('DELETE')
        
            <input type="hidden" name="id" value="{{ $album->id }}">
            <button type="submit" class="btn btn-danger">Удалить</button>
        </form>
    </div>
    <div class="col">
        <div class="d-flex h-100 flex-column justify-content-center">
            <h4>Предпросмотр</h4>

            <div class="card mb-3 row shadow-sm" style="max-width: 540px;">
                <div class="row g-0" style="max-height: 200px">
                    <div class="col-md-4" style="max-height: 200px">
                        <img id="lookoutPicture"
                            src="{{ Storage::disk('images')->url($album->image) }}"
                            class="img-fluid rounded-start h-100" alt="Тут будет обложка">
                    </div>
                    <div class="col-md-8" style="max-height: 200px">
                        <div class="card-body">
                            <h5 class="card-title" id="lookoutName"></h5>
                            <h6 class="card-text" id="lookoutArtist"></h6>
                            <p class="card-text" id="lookoutDescription"
                                style="max-height: 100px; text-overflow: ellipsis; overflow: hidden">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/albums-app.js"></script>
@endsection
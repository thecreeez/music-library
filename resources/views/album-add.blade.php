@extends('parts.app')

@section('title')Добавление альбома@endsection

@section('content')
<div class="row">
    <div class="col">
        <h4>Добавить альбом</h4>
        <form action="{{ route('album') }}" method="post">
            @csrf
            @method('PUT')

            <div class="mb-3">
                <label for="inputArtist" class="form-label">Исполнитель</label>
                <input type="text" class="form-control" id="inputArtist" name="artist" value="{{ old('artist') }}">
            </div>
            <div class="mb-3">
                <label for="inputName" class="form-label">Альбом</label>
                <input type="text" class="form-control" id="inputName" name="name" data-bs-toggle="dropdown" value="{{ old('name') }}">
                <ul class="dropdown-menu" aria-labelledby="inputName" id="namesByApi"></ul>
            </div>
            <div class="mb-3">
                <label for="inputDescription" class="form-label">Описание</label>
                <textarea class="form-control" id="inputDescription" rows="3" name="description">{{ old('description') }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
    <div class="col">
        <div class="d-flex h-100 flex-column justify-content-center">
            <h4>Предпросмотр</h4>

            <div class="card mb-3 row shadow-sm" style="max-width: 540px;">
                <div class="row g-0" style="max-height: 200px">
                    <div class="col-md-4" style="max-height: 200px">
                        <img id="lookoutPicture"
                            src="{{ Storage::disk('images')->url('placeholder.png') }}"
                            class="img-fluid rounded-start h-100" alt="Тут будет обложка">
                    </div>
                    <div class="col-md-8" style="max-height: 200px">
                        <div class="card-body">
                            <h5 class="card-title" id="lookoutName">Тут будет ваше прекрасное название</h5>
                            <h6 class="card-text" id="lookoutArtist">А тут ваш замечательный исполнитель</h6>
                            <p class="card-text" id="lookoutDescription"
                                style="max-height: 100px; text-overflow: ellipsis; overflow: hidden">
                                А здесь? Конечно же описание пластинки!!!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/albums-app.js"></script>
@endsection
<?php

namespace Database\Seeders;

use \App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        User::factory(5)->create();

        User::factory()->create([
            'login' => 'Billy'
        ]);

        User::factory()->create([
            'login' => 'Stalker2010'
        ]);

        User::factory()->create([
            'login' => 'mef228228'
        ]);
    }
}

const INPUT_NAME = document.getElementById("inputName");
const LOOKOUT_NAME = document.getElementById("lookoutName");

const INPUT_ARTIST = document.getElementById("inputArtist");
const LOOKOUT_ARTIST = document.getElementById("lookoutArtist");

const INPUT_DESCRIPTION = document.getElementById("inputDescription");
const LOOKOUT_DESCRIPTION = document.getElementById("lookoutDescription");

const LOOKOUT_PICTURE = document.getElementById("lookoutPicture");

const LIST_FOR_NAMES_BY_API = document.getElementById("namesByApi");

let timeAfterLastChangeInputName = Date.now();
let lastTimeSearch = null;

let timeoutSearch = 300;

function updateLookout() {
    console.log()
    if (INPUT_NAME.value.length > 0 && INPUT_NAME.value != LOOKOUT_NAME.innerHTML) {
        LOOKOUT_NAME.innerHTML = INPUT_NAME.value;

        timeAfterLastChangeInputName = Date.now();
    }

    if (INPUT_ARTIST.value !== "" && LOOKOUT_ARTIST.innerHTML !== INPUT_ARTIST.value) {
        LOOKOUT_ARTIST.innerHTML = INPUT_ARTIST.value;
    }

    if (INPUT_DESCRIPTION.value !== "" && LOOKOUT_DESCRIPTION.innerHTML !== INPUT_DESCRIPTION.value) {
        LOOKOUT_DESCRIPTION.innerHTML = INPUT_DESCRIPTION.value
    }
}

setInterval(async () => {
    updateLookout();

    if (INPUT_NAME.value.length > 0 && lastTimeSearch !== INPUT_NAME.value && Date.now() - timeAfterLastChangeInputName > timeoutSearch) {
        lastTimeSearch = INPUT_NAME.value;

        let data = await fetch(`/api/search-album?name=${INPUT_NAME.value}`);
        try {
            let dataFormed = await data.json();

            clearNamesByApi();

            dataFormed.forEach((artistData) => {
                addNameByApi(artistData);
            })
        } catch (e) {
            // Не авторизован
        }
    }
}, 100);

function clearNamesByApi() {
    LIST_FOR_NAMES_BY_API.innerHTML = "";
}

function addNameByApi(data) {
    let li = document.createElement("li");
    let div = document.createElement("div");
    div.className = "dropdown-item";

    div.innerHTML = `${data.name}`
    div.data = [data.name, data.image[data.image.length - 1], data.artist];

    div.onclick = useDataFromApi

    li.appendChild(div);
    LIST_FOR_NAMES_BY_API.appendChild(li);
}

async function useDataFromApi(el) {
    let name = el.target.data[0];
    let image = el.target.data[1]["#text"];
    let artist = el.target.data[2];

    INPUT_NAME.value = name
    INPUT_ARTIST.value = artist;
    LOOKOUT_PICTURE.src = image;
}